import random
from pymote.algorithm import NodeAlgorithm
from pymote.message import Message

#TODO
#Napisani statusi i poruke pod time statusima koje treba implementirati iz pseudokoda
#Initiator - Receiving(Ack)
#Awake - Receiving(Expand)
#waiting_for_ack - Receiving(Ack)
#Active - Receiving(Iteration_Complete)
#Active - Receiving(Start_Iteration)
#Active - Receiving(Expand)
#Active - Receiving(Notify)
#Active - Receiving(Terminate)
#Computing - Receiving(MinValue)


class PTConstruction(NodeAlgorithm):
    """
    Construction of the initiator (source) node shortest path routing table.

    It differs from the book in following:

    1. To be able to perform routing table construction selected node was
       added to IterationComplete message data so when source receives
       IterationComplete message it can update its routing table.

    2. At the end all nodes are in status DONE, and source node is INITIATOR

    """
    required_params = ()
    default_params = {
        'neighborsKey': 'Neighbors',
        'routingTableKey': 'routingTable',
        'weightKey': 'weight',
    }

    def initializer(self):
        # set weights
        for node in self.network.nodes():
            node.memory[self.weightKey] = dict()
        for u, v, data in self.network.edges(data=True):
            u.memory[self.weightKey][v] = data[self.weightKey]
            v.memory[self.weightKey][u] = data[self.weightKey]
            #print "ID= ", u.id, "Memorija od u je --> ", u.memory
            #print "ID= ", v.id, "Memorija od v je --> ", v.memory
            #print "DATA: ", data
            #print "U --> ", u.memory[self.weightKey][v]
            #print "V --> ", v.memory[self.weightKey][u]
        # set statuses and neighbors
        for node in self.network.nodes():
            sensor_readings = node.compositeSensor.read()
            node.memory[self.neighborsKey] = sensor_readings['Neighbors']
            node.status = 'IDLE'
        ini_node = random.choice(self.network.nodes())
        ini_node.status = 'INITIATOR'

        # send Spontaneously
        self.network.outbox.insert(0, Message(
            header=NodeAlgorithm.INI,
            destination=ini_node
        ))

    def initiator(self, node, message):
        #ako si inicijator
        if message.header == NodeAlgorithm.INI:
            #postavi source na true
            node.memory['Source'] = True
            #postavi svoju udaljenost na 0
            node.memory['My_distance'] = 0
            #postavi ack count na broj susjeda
            node.memory['akcount'] = len(node.memory[self.neighborsKey])
            node.memory['Unvisited'] = node.memory[self.neighborsKey][:]           
            node.memory['Children'] = []
            #posalji Notify svim susjedima
            node.send(Message(
                header="Notify",
                destination=node.memory[self.neighborsKey]
            ))
        #ako si primio ack
        #TREBA DOVRSIT !! - dovrseno, valjda
        if message.header == "Ack":
            node.memory['akcount'] -= 1
            if node.memory['akcount'] == 0:
                node.memory['Iteration'] = 1
                self.compute_local_minimum(node, message)
                node.memory['Path_length'] = node.memory['Minpath']
                node.memory['Children'].append(node.memory['Mychoice'])
         
                node.send(Message(
                    header="Expand",
                    data = (node.memory['Iteration'], node.memory['Path_length']),
                    destination=node.memory['Mychoice'])) #DODAJ CHILD U DESTINATION
                node.memory['Unvisited'].remove(node.memory['Mychoice']) #MAKNI DIJETE IZ MEMORIJE
                node.status = "ACTIVE"
    
    def idle(self, node, message):
        if message.header == "Notify":
            #postavi unvisited na susjede
            #izbaci onog od koga si primio poruku
            node.memory['Unvisited'] = node.memory[self.neighborsKey][:]
            node.memory['Unvisited'].remove(message.source)
            #send "Ack" to sender
            node.send(Message(
                header='Ack',
                destination=message.source,
            ))
            #become "Awake"
            node.status = 'AWAKE'

    def awake(self, node, message):
        if message.header == "Expand":
            node.memory['My_distance'] = message.data[1]
            node.memory['Parent'] = message.source
            node.memory['Children'] = []
            if len(node.memory[self.neighborsKey]) > 1:
                destination_nodes = node.memory[self.neighborsKey][:]
                #print "\nDEST NODES ", destination_nodes, "SOURCE ", message.source
                destination_nodes.remove(message.source) 
                node.send(Message(
                    destination=destination_nodes,
                    header='Notify'
                ))
                number_of_neighbors = len(node.memory[self.neighborsKey])
                node.memory['akcount'] = number_of_neighbors - 1
                node.status = 'WAITING_FOR_ACK'
            else:
                node.send(Message(
                    destination=node.memory['Parent'],
                    header='Iteration_Completed'
                ))
                node.status = 'ACTIVE'

        if message.header == "Notify":
            #remove the source of the message from Unvisited
            node.memory['Unvisited'].remove(message.source)
            #send "Ack" to sender
            node.send(Message(
                destination=message.source,
                header='Ack'
            ))

    def waiting_for_ack(self, node, message):
        if message.header == "Ack":
            node.memory['akcount'] -= 1
            if node.memory['akcount'] == 0:
                node.send(Message(
                    destination=node.memory['Parent'],
                    header='Iteration_Completed'
                ))  
                node.status = "ACTIVE"  

    def active(self, node, message):
        if message.header == "Iteration_Completed":
        	#TODO PROVJERI DAL RADI
            if not node.memory['Source']:
                node.send(Message(
                    destination=node.memory['Parent'],
                    header='Iteration_Completed'
                ))
            else:
                node.memory['Iteration'] += 1
                node.send(Message(
                    destination=node.memory['Children'],
                    header='Start_Iteration',
                    data = node.memory['Iteration']
                ))  
                self.compute_local_minimum(node, message)
                node.memory['Childcount'] = 0
                node.status = 'COMPUTING'

        if message.header == "Start_Iteration":
            #postai iteration u memoriji na iteration iz poruke
            node.memory['Iteration'] = message.data ###TODO provjeri kako je zapisan iteration u prouci
            self.compute_local_minimum(node, message)
            if len(node.memory['Children']) == 0:
                node.send(Message(
                    destination=node.memory['Parent'],
                    header='MinValue',
                    data = node.memory["Minpath"]
                ))
            else:
                node.send(Message(
                    destination=node.memory['Children'],
                    header='Start_Iteration',
                    data = node.memory['Iteration']
                ))
                node.memory['Childcount'] = 0
                node.status = 'COMPUTING' 

        if message.header == "Expand":
            node.send(Message(
                destination=node.memory['Exit'],
                header='Expand',
                data = (node.memory['Iteration'], node.memory['Path_value'])
            ))  
            if node.memory['Exit'] == node.memory['Mychoice']:
                node.memory['Children'].append(node.memory['Mychoice'])
                node.memory['Unvisited'].remove(node.memory['Mychoice'])
            
        if message.header == "Notify":
            node.memory['Unvisited'].remove(message.source)
            node.send(Message(
                destination=message.source,
                header='Ack'
            )) 
        if message.header == "Terminate":
        	#TODO moze li se slati na vise djece istovremeno ? (destination)
            node.send(Message(
                destination=node.memory['Children'],
                header='Terminate'
            ))
            node.status="DONE"

    def computing(self, node, message):
        if message.header == "MinValue":
            if message.data < node.memory['Minpath']:
                node.memory['Minpath'] = message.data
                node.memory['Exit'] = message.source
            node.memory['Childcount'] += 1
            if node.memory['Childcount'] == len(node.memory['Children']):
                if not node.memory['Source']:
                    node.send(Message(
                        destination=node.memory['Parent'],
                        header='MinValue',
                        data = node.memory['Minpath']
                    ))
                    node.status = 'ACTIVE'
                else:
                    #check for termination
                    self.compute_local_minimum(node, message)

    def done(self, node, message):
        pass

    # Procedures
    def check_for_termination(self, node, message):
        if node.memory['Minpath'] == float("inf"):
            node.send(Message(
                destination=node.memory['Children'],
                header='Terminate'
                )) 
            node.status = "DONE"
        else:
            node.send(Message(
                destination=node.memory['Exit'],
                header="Expand",
                data=(node.memory['Iteration'], node.memory['Minpath'])
                )) 
            node.status = "ACTIVE"           


    def compute_local_minimum(self, node, message):
        if len(node.memory['Unvisited']) == 0:
            node.memory['Minpath'] = float("inf")
        else:
            link_length = float("inf")
            
            #TODO provjeriti kako tocno pristupiti memoriji za dobiti id i tezinu
            for unvisited_neighbour in node.memory['Unvisited']:
                current = node.memory[self.weightKey][unvisited_neighbour]
                if current < link_length:
                    link_length = current
                    y = unvisited_neighbour
            node.memory['Minpath'] = node.memory['My_distance'] + link_length
            node.memory['Mychoice'] = y
            node.memory['Exit'] = y
            

    STATUS = {
        'INITIATOR': initiator,
        'IDLE': idle,
        'AWAKE': awake,
        'WAITING_FOR_ACK': waiting_for_ack,
        'ACTIVE': active,
        'COMPUTING': computing,
        'DONE': done
    }
